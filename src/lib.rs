///
use std::default::Default;
use std::error::Error;
use std::fs::File;
use std::io::Read;

use syn::parse_quote;
use syn::visit::{self, Visit};
use syn::FnArg;

use quote::ToTokens;

//
// Note: Originally my plan was to just store these as
//       strings but it ended up being easier to store
//       the `syn::Lit` instead.
//
// TODO: Re-evaluate this choice at some point if e.g. strings
//       lead to easier/lower overhead inter-op.
//
// TODO: Revisit visibility of fields etc.
//
#[derive(Default, Debug, Clone)]
pub struct ParamConfig {
    name: Option<String>,
    min: Option<syn::Lit>,
    max: Option<syn::Lit>,
    default: Option<syn::Lit>,
}

impl ParamConfig {
    pub fn update_from_meta_name_value(&mut self, mnv: &syn::MetaNameValue) {
        //
        if let Some(ident) = mnv.path.get_ident() {
            match ident.to_string().as_str() {
                "default" => self.default = Some(mnv.lit.clone()),
                "min" => self.min = Some(mnv.lit.clone()),
                "max" => self.max = Some(mnv.lit.clone()),

                _ => {}
            }
        }
    }

    // TODO: Handle the following more nicely?

    pub fn name(&self) -> String {
        self.name.as_ref().map(|i| i.clone()).unwrap_or_default()
    }

    pub fn default(&self) -> String {
        self.default.to_token_stream().to_string()
    }

    pub fn min(&self) -> String {
        self.min.to_token_stream().to_string()
    }

    pub fn max(&self) -> String {
        self.max.to_token_stream().to_string()
    }
}

//
// Given what we're doing I'm not sure that it makes
// sense to use the visitor approach here but it works
// (finally!) for now.
//
// TODO: Document visitor + meta lifetime issue/solution?
//
// TODO: Re-evaluate use of visitor approach?
//
#[derive(Default)]
struct FnVisitor {
    items: Vec<syn::MetaNameValue>,
    params: Vec<ParamConfig>,
}

impl<'ast> visit::Visit<'ast> for FnVisitor {
    //

    fn visit_item_fn(&mut self, node: &'ast syn::ItemFn) {
        for attr in &node.attrs {
            if attr == &parse_quote!(#[fj::model]) {
                eprintln!(
                    "found `fj::model` function: {}",
                    &node.sig.ident.to_string()
                );

                //
                // Note: It wasn't initially clear to me but
                //       we need to call the original implementation
                //       if we want *all* of the other handling
                //       to be run--it's not just to handle the
                //       e.g. "nested function" case.
                //
                syn::visit::visit_item_fn(self, node);
            }
        }
    }

    fn visit_fn_arg(&mut self, node: &'ast syn::FnArg) {
        //

        if let FnArg::Typed(arg) = node {
            for attr in &arg.attrs {
                //
                // Looks for `value` attributes of the form seen here:
                //
                //     #[fj::model]
                //     pub fn model(
                //         #[value(default = 5, min = 3)] num_points: u64,
                //         #[value(default = 1.0, min = 1.0)] r1: f64,
                //         #[value(default = 2.0, min = 2.0)] r2: f64,
                //         #[value(default = 1.0)] h: f64,
                //     ) -> fj::Shape {
                //         // ...
                //     }
                //
                if Some(&parse_quote!(value)) == attr.path.get_ident() {
                    //

                    if let syn::Pat::Ident(ident) = arg.pat.as_ref() {
                        //

                        let name = Some(ident.ident.to_string());

                        eprintln!(
                            "    found arg with value attribute: {:}",
                            name.to_token_stream()
                        );

                        self.params.push(ParamConfig {
                            name,
                            ..Default::default()
                        });

                        if let Ok(ref meta) = attr.parse_meta() {
                            self.visit_meta(meta); // !!! TODO: Add comment about this.
                        }

                        syn::visit::visit_fn_arg(self, node);
                    }
                }
            }
        }
    }

    fn visit_meta_name_value(&mut self, i: &'ast syn::MetaNameValue) {
        //

        self.items.push(i.clone());

        self.params
            .as_mut_slice()
            .iter_mut()
            .last()
            .expect("Unexpectedly empty params list. At least one `ParamConfig` should have been added by `visit_fn_arg()`.")
            .update_from_meta_name_value(i);

        syn::visit::visit_meta_name_value(self, i);
    }
}

pub fn from_file_path(file_path: &str) -> Result<Vec<ParamConfig>, Box<dyn Error>> {
    //

    let mut content = String::new();
    let mut file = File::open(file_path)?;
    file.read_to_string(&mut content)?;

    let model_source_ast = syn::parse_file(&content)?;

    let mut visitor = FnVisitor::default();
    visitor.visit_file(&model_source_ast);

    Ok(visitor.params)
}
