use std::env;
use std::error::Error;

use fj_proto_param_cfg;

fn main() -> Result<(), Box<dyn Error>> {
    //

    println!();

    let file_path = env::args()
        .nth(1)
        .expect("Please supply a file path to the model `lib.rs` file.");

    println!("file path: {file_path:?}");

    println!();

    let model_params = fj_proto_param_cfg::from_file_path(&file_path)?;

    println!();

    println!(
        "{:>20} | {:>8} | {:>8} | {:>8} |",
        "param", "default", "min", "max"
    );

    println!("    ------------------------------------------------------");

    for cfg in model_params {
        println!(
            "{:>20} | {:>8} | {:>8} | {:>8} |",
            &cfg.name(),
            &cfg.default(),
            &cfg.min(),
            &cfg.max(),
        );
    }

    println!();

    Ok(())
}
